﻿
using System;
using Carton.Graphics;
using OpenTK;
using OpenTK.Graphics;

namespace CartonGUI
{
    public class GUISprite 
    {

        public GUISprite(GUIWindow window, Material material)
        {
            Vertices vertices = new Vertices();
            vertices.AddRange(new Vertex[]
            {
                new Vertex(new Vector3(-0.5f, 0.5f, 0.0f), Color4.White),
                new Vertex(new Vector3(-0.5f, -0.5f, 0.0f), Color4.Red),
                new Vertex(new Vector3(0.5f, -0.5f, 0.0f), Vector4.One),
                new Vertex(new Vector3(0.5f,  0.5f, 0.0f), Vector4.One),

            }, new uint[]
            {
                0, 1, 2, 2, 3, 0
            });


            window.Renderer.AddVertices(vertices, material);
        }

    }
}
