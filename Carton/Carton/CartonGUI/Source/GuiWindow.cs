﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Carton;
using Carton.Graphics;

namespace CartonGUI
{
    /// <summary>
    /// Window used by the editor (this is not the build window).
    /// </summary>
    public class GUIWindow : Window
    {
        internal Renderer Renderer;

        /// <summary>
        /// Creates a new GUI window.
        /// </summary>
        public GUIWindow()
            : base()
        {
            Renderer = new Renderer();    
        }

        protected override void OnRenderFrame()
        {
            Renderer.RenderVertexBatches();
        }
    }
}
