﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics;

namespace CartonGUI
{
    /// <summary>
    /// Manages editor windows.
    /// </summary>
    public class GUIWindowManager : IDisposable
    {
        List<GUIWindow> _windows = new List<GUIWindow>();

        /// <summary>
        /// Whether or not all editor windows have closed.
        /// </summary>
        public bool AllWindowsHaveClosed { get; private set; }

        /// <summary>
        /// Adds a new editor window.
        /// </summary>
        /// <param name="window"></param>
        public void Add(GUIWindow window)
        {
            window.Closing += (object sender, System.ComponentModel.CancelEventArgs e) => 
            {
                if (!e.Cancel)
                {
                    _windows.Remove(window);
                    window.Dispose();
                }                
            };
            
            _windows.Add(window);
        }

        /// <summary>
        /// Updates all dirty editor windows.
        /// </summary>
        public void UpdateWindows()
        {
            foreach(GUIWindow window in _windows)
            {
                window.ProcessEvents();
                window.UpdateFrame();
                window.RenderFrame();
                GraphicsContext.CurrentContext.SwapBuffers();
            }
        }

        /// <summary>
        /// Closes and disposes all editor windows.
        /// </summary>
        public void Dispose()
        {
            for (int i = 0; i < _windows.Count; i++)
            {
                _windows[i].Close();
            }
        }
    }
}
