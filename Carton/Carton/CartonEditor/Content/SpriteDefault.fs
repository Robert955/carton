#version 330

layout(location = 0) out vec4 out_color;

in vec4 v2f_color;

void main()
{
    out_color = v2f_color;
}