﻿
using System;
using CartonEditor.Windows;
using CartonGUI;

namespace CartonEditor
{
    /// <summary>
    /// Entry class for the editor.
    /// </summary>
    public class EditorLauncher : IDisposable
    {
        GUIWindowManager _windowManager;

        /// <summary>
        /// Entry point for the editor.
        /// </summary>
        public void Launch()
        {
            Carton.Application.Init();
            _windowManager = new GUIWindowManager();

            _windowManager.Add(new MasterWindow());
            _windowManager.Add(new SlaveWindow());
            
            while (!_windowManager.AllWindowsHaveClosed)
            {
                _windowManager.UpdateWindows();
            }
        }

        /// <summary>
        /// Disposes disposable objects in the editor launcher.
        /// </summary>
        public void Dispose()
        {
            _windowManager.Dispose();
        }
    }
}
