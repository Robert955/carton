﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using CartonGUI;
using Carton;
using Carton.Graphics;
using System.IO;
using System;

namespace CartonEditor.Windows
{
    /// <summary>
    /// The main window (all other windows are children of this window).
    /// </summary>
    public class MasterWindow : GUIWindow
    {
        Texture2D texture;
        Shader vertexDiffuseShader;
        Shader fragmentDiffuseShader;
        Material material;
        GUISprite sprite;
        
        internal MasterWindow()
            : base()
        {
            Title = "Carton Master";
            X -= (Width / 2);

            texture = AssetLoader.Load<Texture2D>("SomeTexture.jpg", new Texture2DLoadOptions(
                TextureMinFilter.Nearest, 
                TextureMagFilter.Nearest, 
                TextureWrapMode.Repeat, 
                TextureWrapMode.Repeat));

             material = AssetLoader.Load<Material>("Default.mat");

             vertexDiffuseShader = AssetLoader.Load<Shader>("SpriteDefault.vs", new ShaderLoadOptions(ShaderType.VertexShader));

             fragmentDiffuseShader = AssetLoader.Load<Shader>("SpriteDefault.fs", new ShaderLoadOptions(ShaderType.FragmentShader));

             material.VertexShader = vertexDiffuseShader;
             material.FragmentShader = fragmentDiffuseShader;
            

            sprite = new GUISprite(this, material);
        }
    }
}
