﻿using OpenTK.Graphics.OpenGL;
using CartonGUI;

namespace CartonEditor.Windows
{
    public class SlaveWindow : GUIWindow
    {
        internal SlaveWindow()
            : base()
        {
            Title = "Carton Slave";
            X += (Width / 2);
            
        }

        protected override void OnRenderFrame()
        {
            base.OnRenderFrame();
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.ClearColor(0.0f, 0.2f, 0.5f, 1.0f);

            GL.Begin(PrimitiveType.Lines);
            GL.Vertex2(0.0f, 0.0f);
            GL.Vertex2(1.0f, 1.0f);
            GL.End();
        }
    }
}
