﻿
namespace CartonEditor
{
    class Program
    {
        static void Main(string[] args)
        {
            using (EditorLauncher launcher = new EditorLauncher())
            {
                launcher.Launch();
            }
        }
    }
}
