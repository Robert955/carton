﻿
using System;
using System.Runtime.ExceptionServices;
using OpenTK;

namespace Carton
{
    /// <summary>
    /// Carton application.
    /// </summary>
    public static class Application
    {
        /// <summary>
        /// Whether or not the Carton application has been initialized.
        /// </summary>
        public static bool Initialized { get; private set; }

        /// <summary>
        /// Initializes all systems to be used by Carton.
        /// Call this before using any kind of Carton functionality.
        /// </summary>
        public static void Init()
        {
            AppDomain.CurrentDomain.FirstChanceException += FirstChanceExceptionHandler;

            Log.Init();

            Toolkit.Init(
                new ToolkitOptions
                {
                    Backend = PlatformBackend.PreferNative,
                    EnableHighResolution = true
                });

            InitDone();
        }

        private static void FirstChanceExceptionHandler(object sender, FirstChanceExceptionEventArgs e)
        {
            Log.Error(e.Exception.ToString());
        }

        private static void InitDone()
        {
            Log.Message("Carton Initialized.");
            Initialized = true;
        }
    }
}
