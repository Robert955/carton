﻿
using System.Collections.Generic;
using System.IO;

namespace Carton
{
    /// <summary>
    /// Loads and manages assets.
    /// </summary>
    public static class AssetLoader
    {
        private const string RootFolder = "Content/";
        private static Dictionary<string, Asset> _assets = new Dictionary<string, Asset>();

        /// <summary>
        /// Loads an asset with the default load options, returns an existing asset immidiately if its already loaded.
        /// </summary>
        /// <typeparam name="T"> Type of the asset. </typeparam>
        /// <param name="assetPath"> Path of the asset within the content folder. </param>
        /// <returns></returns>
        public static T Load<T>(string assetPath) where T : Asset, new()
        {
            return Load<T>(assetPath, null);
        }

        /// <summary>
        /// Loads an asset with custom load options, returns an existing asset immidiately if its already loaded.
        /// </summary>
        /// <typeparam name="T"> Type of the asset. </typeparam>
        /// <param name="assetPath"> Path of the asset within the content folder. </param>
        /// <param name="options"> Options for loading the asset. </param>
        /// <returns></returns>
        public static T Load<T>(string assetPath, AssetLoadOptions options) where T : Asset, new()
        {
            string fullPath = RootFolder + assetPath;
            if (_assets.ContainsKey(fullPath))
                return (T)_assets[fullPath];

            if (!Directory.Exists(RootFolder))
                Directory.CreateDirectory(RootFolder);
                                       
            T asset = new T();
            asset.Name = fullPath;
            _assets.Add(fullPath, asset);

            if (options != null)
            {
                if (options.AssetType != typeof(T))
                {
                    Log.Error(string.Format(
                        "Asset type of options '{0}' is '{1}' but '{2}' was expected.",
                        options.GetType().Name, options.AssetType.Name, typeof(T).Name));
                }
            }

            asset.Load(options);
            return asset;
        }
    }
}
