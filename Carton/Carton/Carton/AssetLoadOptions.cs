﻿
namespace Carton
{
    /// <summary>
    /// Base options class for loading an asset.
    /// </summary>
    public abstract class AssetLoadOptions
    {
        /// <summary>
        /// The type of the asset these options apply to.
        /// </summary>
        public abstract System.Type AssetType { get; }
    }
}
