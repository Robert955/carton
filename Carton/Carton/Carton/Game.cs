﻿namespace Carton
{
    /// <summary>
    /// The static core of a game.
    /// </summary>
    public static class Game 
    {
        /// <summary>
        /// The window used by the game.
        /// </summary>
        public static Window GameWindow { get; private set; }

        static bool _isRunning;
        /// <summary>
        /// Whether or not the game is currently running.
        /// </summary>
        public static bool IsRunning { get { return _isRunning; } }

        static bool _isQuiting;
        /// <summary>
        /// Whether or not the game currently quiting.
        /// </summary>
        public static bool IsQuiting { get { return _isQuiting; } }

        /// <summary>
        /// Quits the game.
        /// </summary>
        public static void Quit()
        {
            _isRunning = false;
            _isQuiting = true;
        }

        /// <summary>
        /// Launches the game and calls OnLaunch.
        /// </summary>
        public static void Launch()
        {
            if (IsRunning || IsQuiting)
            {
                Log.Error("The game has already been launched.");
                return;
            }

            GameWindow = new Window();
            _isRunning = true;

            while (IsRunning)
            {
                GameWindow.ProcessEvents();
                UpdateFrame();
                RenderFrame();
            }
            GameWindow.Dispose();
        }
        
        static void RenderFrame()
        {
            // TODO: render a frame.
            GameWindow.RenderFrame();
            OpenTK.Graphics.GraphicsContext.CurrentContext.SwapBuffers();
        }
        
        static void UpdateFrame()
        {
            // TODO: update a frame.
        }
    }
}
