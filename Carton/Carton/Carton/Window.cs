﻿using Carton.Graphics;
using OpenTK;
using OpenTK.Graphics;

namespace Carton
{
    /// <summary>
    /// The carton window class.
    /// </summary>
    public class Window : NativeWindow
    {
        /// <summary>
        /// The graphics context for this window.
        /// </summary>
        public GraphicsContext Context { get; private set; }

        /// <summary>
        /// The OpenGL renderer for this window.
        /// </summary>
        public Renderer Renderer { get; private set; }

        /// <summary>
        /// Creates a new window.
        /// </summary>
        public Window()
            : base()
        {
            Visible = true;
            Context = new GraphicsContext(GraphicsMode.Default, WindowInfo);
            Context.LoadAll();
            Renderer = new Renderer();
        }

        /// <summary>
        /// Override to add rendering code.
        /// </summary>
        protected virtual void OnRenderFrame() { }

        /// <summary>
        /// Renders a frame to the window and calls OnRenderFrame.
        /// </summary>
        public void RenderFrame()
        {
            if (!Context.IsCurrent)
                Context.MakeCurrent(WindowInfo);
            Renderer.RenderVertexBatches();
            OnRenderFrame();
        }

        /// <summary>
        /// Override to add code that has to happen every frame.
        /// </summary>
        protected virtual void OnUpdateFrame() { }

        /// <summary>
        /// Updates a frame and calls OnUpdateFrame.
        /// </summary>
        public void UpdateFrame()
        {
            OnUpdateFrame();
        }
    }
}
