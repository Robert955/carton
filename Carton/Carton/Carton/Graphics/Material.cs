﻿
using System;
using OpenTK.Graphics.OpenGL4;
using System.Runtime.InteropServices;
using OpenTK;

namespace Carton.Graphics
{
    public class Material : Asset
    {
        /// <summary>
        /// The texture use by this material (optional).
        /// </summary>
        public Texture2D Texture { get; set; }
        
        private Shader _vertexShader;
        /// <summary>
        /// The vertex shader used by this material.
        /// </summary>
        public Shader VertexShader
        {
            get { return _vertexShader; }
            set
            {
                if(_vertexShader != value)
                {
                    UpdateShader(_vertexShader, value);
                }
                _vertexShader = value;
            }
        }

        private Shader _fragmentShader;
        /// <summary>
        /// The fragment shader used by this material.
        /// </summary>
        public Shader FragmentShader
        {
            get { return _fragmentShader; }
            set
            {
                if (_fragmentShader != value)
                {
                    UpdateShader(_fragmentShader, value);
                }
                _fragmentShader = value;
            }
        }

        internal int Program { get; private set; }

        private void UpdateShader(Shader oldShader, Shader newShader)
        {
            if (oldShader != null)
            {
                oldShader.Detach(Program);
            }

            if (newShader != null)
            {
                newShader.Attach(Program);
            }
        }

        internal override void Load(AssetLoadOptions options)
        {
            Vector2[] vertices = {
                 new Vector2(0.0f,  0.5f), // Vertex 1 (X, Y)
                 new Vector2(0.5f, -0.5f), // Vertex 2 (X, Y)
                 new Vector2(-0.5f, -0.5f)  // Vertex 3 (X, Y)
            };

            Program = GL.CreateProgram();
        }
        
        internal void Bind()
        {
            GL.UseProgram(Program);
        }

        internal void Unbind()
        {
            GL.UseProgram(0);
        }

        internal void BindAttributes()
        {
            if (VertexShader != null)
                VertexShader.BindAttributes(Program);
            if (FragmentShader != null)
                FragmentShader.BindAttributes(Program);
        }
    }
}
