﻿
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL4;
using System.Runtime.InteropServices;
using System;

namespace Carton.Graphics
{
    public class Renderer
    {
        Dictionary<Material, VertexBatch> _batches = new Dictionary<Material, VertexBatch>();
        
        /// <summary>
        /// Creates a new renderer.
        /// </summary>
        public Renderer()
        {
            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.Texture2D);
            GL.DebugMessageCallback(OpenGLDebugMessageHandler, IntPtr.Zero);
        }

        void OpenGLDebugMessageHandler(DebugSource source, DebugType type, int id, DebugSeverity severity, int length, IntPtr message, IntPtr userParam)
        {
            string msg = string.Format("OpenGL [{0}][{1}]: {2}", source.ToString(), type.ToString(), message);
            switch (severity)
            {
                case DebugSeverity.DebugSeverityNotification:
                    Log.Message(msg);
                    break;
                case DebugSeverity.DebugSeverityHigh:
                    Log.Error(msg);
                    break;
                case DebugSeverity.DebugSeverityMedium:
                    Log.Error(msg);
                    break;
                case DebugSeverity.DebugSeverityLow:
                    Log.Error(msg);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Adds the vertices to be rendered.
        /// </summary>
        /// <param name="vertices"> The vertices to be rendered. </param>
        /// <param name="material"> The material used for drawing the vertices. </param>
        public void AddVertices(Vertices vertices, Material material)
        {            
            if (_batches.ContainsKey(material))
            {
                _batches[material].Vertices.AddRange(vertices);
                return;
            }
            _batches.Add(material, new VertexBatch(vertices, material));
        }

        /// <summary>
        /// Adds one vertex to be rendered.
        /// </summary>
        /// <param name="vertex"> The vertex to be rendered. </param>
        /// <param name="material">The material used for drawing the vertex. </param>
        public void AddVertex(Vertex vertex, Material material)
        {
            if (_batches.ContainsKey(material))
            {
                _batches[material].Vertices.Add(vertex);
                return;
            }
            Vertices vertices = new Vertices();
            vertices.Add(vertex);
            _batches.Add(material, new VertexBatch(vertices, material));
        }

        /// <summary>
        /// Renders the vertex data from all batches to the frame buffer.
        /// </summary>
        public void RenderVertexBatches()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);

            foreach (KeyValuePair<Material, VertexBatch> batch in _batches)
            {
                batch.Value.Render();
            }
        }
    }
}
