﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Linq;

namespace Carton.Graphics
{
    public class VertexBatch
    {
        /// <summary>
        /// The vertices in the batch.
        /// </summary>
        public Vertices Vertices { get; private set; }

        /// <summary>
        /// The material used by this batch.
        /// </summary>
        public Material Material { get; private set; }

        /// <summary>
        /// The vertex array object (VAO) for this batch.
        /// </summary>
        public readonly int VAO;

        /// <summary>
        /// The vertex buffer object (VBO) for this batch.
        /// </summary>
        public readonly int VBO;

        /// <summary>
        /// The index buffer object (IBO) for this batch.
        /// </summary>
        public readonly int IBO;
        
        /// <summary>
        /// Creates a new vertex batch.
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="material"></param>
        public VertexBatch(Vertices vertices, Material material)
        {
            Vertices = vertices;
            Material = material;

            VAO = GL.GenVertexArray();
            VBO = GL.GenBuffer();
            IBO = GL.GenBuffer();
       
            GL.BindVertexArray(VAO);
            
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            
            GL.BufferData(
                BufferTarget.ArrayBuffer, 
                (IntPtr)(vertices.VertexCount * Vector3.SizeInBytes), 
                vertices._vertices.ToArray(), 
                BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, IBO);

            GL.BufferData(
                BufferTarget.ElementArrayBuffer, 
                (IntPtr)(vertices.IndexCount * sizeof(uint)), 
                vertices._indices.ToArray(), 
                BufferUsageHint.StaticDraw);

            Material.BindAttributes();
     
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindVertexArray(0);
        }

        /// <summary>
        /// Renders the batch.
        /// </summary>
        public void Render()
        {
            Material.Bind();
            GL.BindVertexArray(VAO);

            GL.DrawElements(PrimitiveType.Triangles, Vertices.IndexCount, DrawElementsType.UnsignedInt, Vertices._indices.ToArray());
            
            GL.BindVertexArray(0);
            Material.Unbind();
        }
    }
}
