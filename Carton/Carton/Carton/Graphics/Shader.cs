﻿
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.IO;

namespace Carton.Graphics
{
    public class Shader : Asset
    {
        /// <summary>
        /// Handle to the OpenGL shader.
        /// </summary>
        public int ShaderHandle { get; private set; }

        /// <summary>
        /// What type of shader it is.
        /// </summary>
        public ShaderType ShaderType { get; private set; }

        internal void BindAttributes(int program)
        {
            if (ShaderType == ShaderType.VertexShader)
            {
                int ploc = GL.GetAttribLocation(program, "in_position");
                GL.VertexAttribPointer(ploc, 3, VertexAttribPointerType.Float, false, Vertex.SizeInBytes, 0);
                GL.EnableVertexAttribArray(ploc);

                int cloc = GL.GetAttribLocation(program, "in_color");
                GL.VertexAttribPointer(cloc, 4, VertexAttribPointerType.Float, false, Vertex.SizeInBytes, 3 * sizeof(float));
                GL.EnableVertexAttribArray(cloc);
            }
            else if(ShaderType == ShaderType.FragmentShader)
            {

            }
        }

        internal void Attach(int program)
        {
            GL.AttachShader(program, ShaderHandle);
            string shaderInfo = GL.GetShaderInfoLog(ShaderHandle);
            if (!string.IsNullOrEmpty(shaderInfo))
            {
                Log.Warning(string.Format("Compiling shader '{0}' failed: {1}", Name, shaderInfo));
            }

            GL.LinkProgram(program);
            string programInfo = GL.GetProgramInfoLog(ShaderHandle);
            if (!string.IsNullOrEmpty(programInfo))
            {
                Log.Warning(string.Format("Linking shader '{0}' failed: {1}", Name, programInfo));
            }
        }

        internal void Detach(int program)
        {
            GL.DetachShader(program, ShaderHandle);
            string shaderInfo = GL.GetShaderInfoLog(ShaderHandle);
            if (!string.IsNullOrEmpty(shaderInfo))
            {
                Log.Warning(string.Format("Compiling shader '{0}' failed: {1}", Name, shaderInfo));
            }
        }

        internal void Compile()
        {
            GL.CompileShader(ShaderHandle);
            string shaderInfo = GL.GetShaderInfoLog(ShaderHandle);
            if (!string.IsNullOrEmpty(shaderInfo))
            {
                Log.Warning(string.Format("Compiling shader '{0}' failed: {1}", Name, shaderInfo));
            }
        }

        internal override void Load(AssetLoadOptions options)
        {
            ShaderLoadOptions shaderOptions = null;
            if (options == null)
            {
                Log.Error("Can not load shader without shader load options.");
            }
            else
            {
                shaderOptions = (ShaderLoadOptions)options;
            }

            string source = File.ReadAllText(Name);
            ShaderHandle = GL.CreateShader(shaderOptions.ShaderType);
            GL.ShaderSource(ShaderHandle, source);
            ShaderType = shaderOptions.ShaderType;

            Compile();
        }
    }
}
