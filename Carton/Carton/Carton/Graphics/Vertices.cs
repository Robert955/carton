﻿using OpenTK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Carton.Graphics
{
    /// <summary>
    /// IEnumerable container class for vertices and indices.
    /// </summary>
    public class Vertices : IEnumerable<Vertex>
    {
        /// <summary>
        /// The amount of vertices.
        /// </summary>
        public int VertexCount { get { return _vertices.Count; } }

        /// <summary>
        /// The amount of indices.
        /// </summary>
        public int IndexCount { get { return _indices.Count; } }

        internal List<Vertex> _vertices = new List<Vertex>();
        internal List<uint> _indices = new List<uint>();
        
        /// <summary>
        /// Adds vertices to the vertex array.
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="indices"></param>
        public void AddRange(Vertices vertices, uint[] indices = null)
        {
            this.AddRange(vertices, indices);
        }

        /// <summary>
        /// Adds vertices to the vertex array.
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="indices"></param>
        public void AddRange(Vertex[] vertices, uint[] indices = null)
        {
            _vertices.AddRange(vertices);
            if(indices != null)
                _indices.AddRange(indices);
        }

        /// <summary>
        /// Adds a vertex to the vertex array.
        /// </summary>
        /// <param name="vertex"></param>
        /// <param name="index"></param>
        public void Add(Vertex vertex, uint? index = null)
        {
            _vertices.Add(vertex);
            if(index.HasValue)
                _indices.Add(index.Value);
        }

        /// <summary>
        /// Gets the vertex enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Vertex> GetEnumerator()
        {
            return _vertices.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
