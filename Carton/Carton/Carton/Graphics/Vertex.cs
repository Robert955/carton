﻿
using OpenTK;
using OpenTK.Graphics;
using System.Runtime.InteropServices;

namespace Carton.Graphics
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Vertex
    {
        private static int _sizeInBytes = Vector3.SizeInBytes + Vector4.SizeInBytes;
        public static int SizeInBytes { get { return _sizeInBytes; } }

        public Vector3 Position;
        public Vector4 Color;
        //public Vector2 UV;
        
        
        public Vertex(Vector3 position, Vector4 color)
        {
            this.Position = position;
            this.Color = color;
        }

        public Vertex(Vector3 position, Color4 color)
            : this(position, new Vector4(color.R, color.G, color.B, color.A))
        { }
    }
}
