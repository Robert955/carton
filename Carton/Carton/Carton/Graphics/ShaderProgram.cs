﻿
using OpenTK.Graphics.OpenGL4;

namespace Carton.Graphics
{
    public class ShaderProgram
    {
        /// <summary>
        /// Handle to the shader program in OpenGL.
        /// </summary>
        public int Handle { get; private set; }

        public ShaderProgram()
        {
            this.Handle = GL.CreateProgram();
        }

        public void AddShader(Shader shader)
        {
            
        }
    }
}
