﻿using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;

namespace Carton.Graphics
{
    public class Texture2D : Asset
    {
        private int _glHandle;
        /// <summary>
        /// The handle to the texture in OpenGL.
        /// </summary>
        public int Handle { get { return _glHandle; } }

        /// <summary>
        /// The width on the texture.
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// The height of the texture.
        /// </summary>
        public int Height { get; private set; }

        internal override void Load(AssetLoadOptions options)
        {
            Image img = Image.FromFile(Name);
            Bitmap bitmap = new Bitmap(img);

            Texture2DLoadOptions textureOptions = null;
            if (options != null)
            {
                textureOptions = (Texture2DLoadOptions)options;
            }
            else
            {
                textureOptions = Texture2DLoadOptions.Default;
            }
            
            Width = bitmap.Width;
            Height = bitmap.Height;

            GL.GenTextures(1, out _glHandle);
            GL.BindTexture(TextureTarget.Texture2D, _glHandle);

            BitmapData data = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height), 
                ImageLockMode.ReadOnly, 
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(
                TextureTarget.Texture2D, 0, 
                PixelInternalFormat.Rgba,
                data.Width, data.Height, 0, 
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
                PixelType.UnsignedByte, data.Scan0);

            bitmap.UnlockBits(data);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)textureOptions.MinFilter);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)textureOptions.MagFilter);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)textureOptions.WrapU);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)textureOptions.WrapV);
        }
    }
}
