﻿
using OpenTK.Graphics.OpenGL4;
using System;

namespace Carton.Graphics
{
    public class ShaderLoadOptions : AssetLoadOptions
    {
        public override Type AssetType
        {
            get
            {
                return typeof(Shader);
            }
        }

        /// <summary>
        /// What kind of a shader is it.
        /// </summary>
        public readonly ShaderType ShaderType;

        /// <summary>
        /// Creates new load options for a shader.
        /// </summary>
        /// <param name="shaderType"></param>
        public ShaderLoadOptions(ShaderType shaderType)
        {
            this.ShaderType = shaderType;
        }
    }
}
