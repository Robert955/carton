﻿
using System;
using OpenTK.Graphics.OpenGL4;
using Carton.Graphics;

namespace Carton.Graphics
{
    public class Texture2DLoadOptions : AssetLoadOptions
    {
        private static Texture2DLoadOptions _default;
        /// <summary>
        /// The default texture load options.
        /// </summary>
        public static Texture2DLoadOptions Default
        {
            get
            {
                if (_default == null)
                {
                    _default = new Texture2DLoadOptions(
                        TextureMinFilter.Linear,
                        TextureMagFilter.Linear,
                        TextureWrapMode.Repeat,
                        TextureWrapMode.Repeat);
                }
                return _default;
            }
        }

        /// <summary>
        /// The type of the asset these options apply to.
        /// </summary>
        public override Type AssetType
        {
            get
            {
                return typeof(Texture2D);
            }
        }

        /// <summary>
        /// The minification filter.
        /// </summary>
        public readonly TextureMinFilter MinFilter;

        /// <summary>
        /// The magnification filter.
        /// </summary>
        public readonly TextureMagFilter MagFilter;

        /// <summary>
        /// The wrap mode for the U coords.
        /// </summary>
        public readonly TextureWrapMode WrapU;

        /// <summary>
        /// The wrap mode for the V coords.
        /// </summary>
        public readonly TextureWrapMode WrapV;

        /// <summary>
        /// Creates new load options for a Texture2D.
        /// </summary>
        /// <param name="minFilter"> The minification filter. </param>
        /// <param name="magFilter"> The magnification filter. </param>
        /// <param name="wrapU"> The wrap mode for the U coords. </param>
        /// <param name="wrapV"> The wrap mode for the V coords. </param>
        public Texture2DLoadOptions(
            TextureMinFilter minFilter, 
            TextureMagFilter magFilter,
            TextureWrapMode wrapU,
            TextureWrapMode wrapV)
            : base()
        {
            this.MinFilter = minFilter;
            this.MagFilter = magFilter;
            this.WrapU = wrapU;
            this.WrapV = wrapV;
        }
    }
}
