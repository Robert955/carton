﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using OpenTK.Graphics.OpenGL4;

namespace Carton
{
    enum LogType : byte
    {
        Message,
        Warning,
        Error,
    }

    /// <summary>
    /// Utility for logging and handling errors, warnings, exceptions and messages.
    /// </summary>
    public static class Log
    {
        static FileStream _stream;
        static StreamWriter _writer;
        static bool _isInitialized;
        static int _stackFrame;

        /// <summary>
        /// Whether or not the logs will be displayed in the console.
        /// (default = true).
        /// </summary>
        public static bool UseConsole { get; set; }

        /// <summary>
        /// Whether or not the logs will be displayed in the log file.
        /// (default = true).
        /// </summary>
        public static bool UseDumpFile { get; set; }

        /// <summary>
        /// Whether or not executing should pause when an error occurs.
        /// </summary>
        public static bool PauseOnError { get; set; }

        /// <summary>
        /// Whether or not caller info (Class, Method, Line numbers) should be logged for errors.
        /// (default = true).
        /// </summary>
        public static bool LogStackInfoForErrors { get; set; }

        /// <summary>
        /// Whether or not caller info (Class, Method, Line numbers) should be logged for warnings.
        /// (default = true).
        /// </summary>
        public static bool LogStackInfoForWarnings { get; set; }

        /// <summary>
        /// Whether or not caller info (Class, Method, Line numbers) should be logged for messages.
        /// (default = true).
        /// </summary>
        public static bool LogStackInfoForMessages { get; set; }

        internal static void Init()
        {
            UseConsole  = true;
            UseDumpFile = true;

            LogStackInfoForErrors   = true;
            LogStackInfoForWarnings = true;
            LogStackInfoForMessages = true;

            _stackFrame = 2;
            _stream = File.Create("LogDump.txt");
            _writer = new StreamWriter(_stream);
            _writer.WriteLine("-- MESSAGE DUMP --" + _writer.NewLine);
            _writer.Flush();
            _isInitialized = true;
        }

        static void LogAny(string msg, LogType type)
        {
            if (!_isInitialized)
                Init();
            
            if (UseDumpFile)
            {
                LogToDumpFile(msg, type);
            }
            if(UseConsole)
            {
                LogToConsole(msg, type);
            }
        }

        static void LogToDumpFile(string msg, LogType type)
        {
            _writer.WriteLine(msg);
            _writer.Flush();
        }

        static void LogToConsole(string msg, LogType type)
        {
            switch (type)
            {
                case LogType.Message:
                    break;
                case LogType.Warning:
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case LogType.Error:
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                default:
                    break;
            }
            Console.WriteLine(msg);
            Console.ResetColor();
        }

        /// <summary>
        /// Dumps an info message.
        /// </summary>
        /// <param name="message"></param>
        public static void Message(object message)
        {
            string stackInfo = LogStackInfoForMessages ? GetStackInfo() : string.Empty;
            LogAny(string.Format("--> {0} {1}", message.ToString(), stackInfo), LogType.Message);
        }

        /// <summary>
        /// Dumps a warning message.
        /// </summary>
        /// <param name="warningMessage"></param>
        public static void Warning(object warningMessage)
        {
            string stackInfo = LogStackInfoForWarnings ? GetStackInfo() : string.Empty;
            LogAny(string.Format("--> [WARNING]: {0} {1}", warningMessage.ToString(), stackInfo), LogType.Warning);
        }

        /// <summary>
        /// Dumps a non-critical error message.
        /// </summary>
        /// <param name="errorMessage"></param>
        public static void Error(object errorMessage)
        {
            string stackInfo = LogStackInfoForErrors ? GetStackInfo() : string.Empty;
            LogAny(string.Format("--> [ERROR]: {0} {1}", errorMessage.ToString(), stackInfo), LogType.Error);

            if(PauseOnError)
            {
                 // TODO: pause execution.
            }
        }

        internal static void GLErrorCheck()
        {
            ErrorCode errorCode = GL.GetError();
            if (errorCode != ErrorCode.NoError)
            {
                _stackFrame = 3;
                Error(errorCode);
                _stackFrame = 2;
            }
        }

        static string GetStackInfo()
        {
            StackTrace stack        = new StackTrace(true);
            StackFrame lastFrame    = stack.GetFrame(_stackFrame);
            MethodBase method       = lastFrame.GetMethod();
            string className        = method.DeclaringType.ToString();
            string methodName       = method.Name;
            string line             = lastFrame.GetFileLineNumber().ToString();

            if (line == "0")
                line = "?";

            return string.Format("({0}.{1}, line:{2})", className, methodName, line.ToString());
        }
    }
}
