﻿
using System;

namespace Carton
{
    public abstract class Asset
    {
        /// <summary>
        /// The name of the asset.
        /// </summary>
        public string Name { get; internal set; }

        internal abstract void Load(AssetLoadOptions options);
    }
}
